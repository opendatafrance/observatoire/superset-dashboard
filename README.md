# Observatoire de l'OpenData – Dashboard

Basé sur https://github.com/amancevice/superset

## Dépendances

Installer `docker` et `docker-compose`.

Générer la base de données SQLite `superset/odservatoire.sqlite` depuis le dépôt https://git.opendatafrance.net/observatoire/indicateurs. Cette étape sera automatisée prochainement.

## Utilisation

Démarrer :

```bash
docker-compose up
```

Ouvrir http://localhost:8088/ et s'authentifier avec `admin`, mot de passe `admin`.

Arrêter :

```bash
docker-compose down
```

## Déploiement en production

Service systemd : TODO

## Initialisation

Cette section est là pour mémoire, elle n'a pas à être réexécutée.

Vu ici: https://github.com/amancevice/superset/tree/master/examples#sqlite

À faire une fois, pour initialiser la base de données SQLite [`superset.db`](./superset/superset.db) :

```bash
# Start Redis service
docker-compose up -d redis
# Wait for services to come up fully...

# Touch SQLite db file
mkdir -p superset
touch superset/superset.db

# Start Superset
docker-compose up -d superset
# Wait for Superset to come up fully...

# Initialize demo
docker-compose exec superset superset-init

# Play around in demo...

# Bring everything down
docker-compose down -v
```

Des erreurs sont apparues, je les ai ignorées. Cf issues https://github.com/apache/incubator-superset/issues/3758 et https://github.com/amancevice/superset/issues/62

Après que la base de données SQLite ait été initalisée, pour démarrer le dashboard, il suffit de faire comme indiqué dans la section [Utilisation](#utilisation).

```bash
docker-compose up
```
